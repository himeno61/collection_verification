import com.lbinkiewicz.exceptions.lexer.IllegalNameException;
import com.lbinkiewicz.exceptions.lexer.NameOutOfBandException;
import com.lbinkiewicz.lexer.Scan;
import com.lbinkiewicz.source.Source;
import com.lbinkiewicz.token.Token;
import lombok.extern.log4j.Log4j;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.lbinkiewicz.token.TokenType.END_OF_FILE;

@Log4j
public class ScanTest {


    @Test
    public void givenEmptyFile_thenResultIsEOF() throws NameOutOfBandException, IndexOutOfBoundsException, FileNotFoundException, IllegalNameException {
        Scan scan = new Scan(new Source(new FileInputStream(new File("src/test/resources/emptyFile.txt"))));
        List<Token> tokens = new ArrayList<>();
        Token currentToken = null;
        do {
            try {
                currentToken = scan.getNextToken();
            } finally {
                tokens.add(currentToken);
            }
        } while (currentToken.getTokenType() != END_OF_FILE);
        tokens.forEach(System.out::println);
    }

    @Test
    public void givenFile1_thenResultIsMatchingTokenList() throws NameOutOfBandException, IndexOutOfBoundsException, FileNotFoundException, IllegalNameException {
        Scan scan = new Scan(new Source(new FileInputStream(new File("src/test/resources/file1.txt"))));
        List<Token> tokens = new ArrayList<>();
        Token currentToken = null;
        do {
            try {
                currentToken = scan.getNextToken();
            } finally {
                tokens.add(currentToken);
            }
        } while (currentToken.getTokenType() != END_OF_FILE);
        tokens.forEach(System.out::println);
    }

    @Test
    public void test() throws NameOutOfBandException, IndexOutOfBoundsException, FileNotFoundException, IllegalNameException {
        String str = "df";
        InputStream is = new ByteArrayInputStream( str.getBytes() );
        Scan scan = new Scan( new Source(is));
        List<Token> tokens = new ArrayList<>();
        Token currentToken = null;
        do {
            try {
                currentToken = scan.getNextToken();
            } finally {
                tokens.add(currentToken);
            }
        } while (currentToken.getTokenType() != END_OF_FILE);
        tokens.forEach(System.out::println);
    }
}
