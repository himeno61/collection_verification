import com.lbinkiewicz.database.DataBase;
import com.lbinkiewicz.exceptions.parser.*;
import com.lbinkiewicz.lexer.Scan;
import com.lbinkiewicz.parser.Parser;
import com.lbinkiewicz.source.Source;
import lombok.extern.log4j.Log4j;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

@Log4j
public class ParserTest {

    @Test
    public void parserTest() throws FileNotFoundException, IllegalExpressionException, TokenReadException, UnknownVariableException, InvalidFunctionException, IllegalTypeException, VariableNameExistsException, SQLException, ClassNotFoundException {
        Parser parser = new Parser(new Scan(new Source(new FileInputStream(new File("src/test/resources/file2.txt")))),new DataBase());
        parser.parse();
    }
}
