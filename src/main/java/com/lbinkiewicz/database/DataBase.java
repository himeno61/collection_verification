package com.lbinkiewicz.database;

import lombok.extern.log4j.Log4j;

import java.sql.*;

@Log4j
public class DataBase {
    private Connection connection;

    public DataBase() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:test.db");
//            Statement statement = connection.createStatement();
//            String sql = "CREATE TABLE COMPANY " +
//                    "(ID INT PRIMARY KEY     NOT NULL," +
//                    " NAME           TEXT    NOT NULL, " +
//                    " SALARY         REAL)";
//            statement.executeUpdate(sql);
//            statement.close();
        } catch ( Exception e ) {
            log.error(e.getClass().getName() + ": " + e.getMessage());
            throw  e;
        }
        log.info("Init database successfully");
    }

    public Float count(String exp) throws SQLException {
//        Statement statement= null;
//        statement = connection.createStatement();
//        ResultSet rs = statement.executeQuery(exp);
//        return rs.getFloat("count");
        return 5f;
    }
    public String select(String exp) throws SQLException {
        Statement statement= null;
        statement = connection.createStatement();
//        return statement.executeQuery(exp).toString();
        return exp;
    }

}
