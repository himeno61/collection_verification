package com.lbinkiewicz.main;

import com.lbinkiewicz.gui.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;
import lombok.extern.java.Log;

@Log
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        MainWindow.start(primaryStage);
    }
}
