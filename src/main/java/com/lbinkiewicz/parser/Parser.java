package com.lbinkiewicz.parser;

import com.lbinkiewicz.database.DataBase;
import com.lbinkiewicz.database.DataBaseProcessing;
import com.lbinkiewicz.exceptions.lexer.IllegalNameException;
import com.lbinkiewicz.exceptions.lexer.NameOutOfBandException;
import com.lbinkiewicz.exceptions.parser.*;
import com.lbinkiewicz.lexer.Scan;
import com.lbinkiewicz.token.Token;
import com.lbinkiewicz.token.TokenType;
import lombok.extern.log4j.Log4j;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.sql.SQLException;
import java.util.*;

import static com.lbinkiewicz.token.TokenType.*;

@Log4j
public class Parser {
    private Scan scan;
    private Token lookahead;
    private Map<String, String> collectionMap;
    private Map<String, Float> valueMap;
    private DataBase dataBase;

    public Parser(Scan scan, DataBase dataBase) {
        this.scan = scan;
        this.dataBase = dataBase;
        collectionMap = new HashMap<>();
        valueMap = new HashMap<>();
    }

    public void parse() throws TokenReadException, UnknownVariableException, IllegalTypeException, InvalidFunctionException, IllegalExpressionException, VariableNameExistsException, SQLException {
        nextToken();
        while (!check(END_OF_FILE)) {
            collectionDecl();
            print();
            variableValueChange();
            valueDeclaration();
        }
    }

    private void nextToken() throws TokenReadException {
        try {
            lookahead = scan.getNextToken();
        } catch (NameOutOfBandException | IllegalNameException e) {
            e.printStackTrace();
            log.error("Token reading error!");
            throw new TokenReadException(e);
        }
    }

    private Token accept(TokenType tokenType) throws TokenReadException, IllegalExpressionException {
        if (lookahead.getTokenType().equals(tokenType)) {
            Token temp = lookahead;
            nextToken();
            return temp;
        }
        throw new IllegalExpressionException(lookahead, tokenType);
    }

    private boolean check(TokenType tokenType) {
        return lookahead.getTokenType().equals(tokenType);
    }

    private void collectionDecl() throws IllegalExpressionException, TokenReadException, UnknownVariableException, IllegalTypeException, InvalidFunctionException, VariableNameExistsException {
        if (lookahead.getTokenType() != COLLECTION)
            return;
        String name = readVariableName(COLLECTION);
        String expression = collectionAssignment();
        accept(SEMICOLON);
        collectionMap.put(name, expression);
    }

    private String collectionAssignment() throws TokenReadException, IllegalExpressionException, IllegalTypeException, UnknownVariableException, InvalidFunctionException {
        if (check(ASSIGN_SIGN)) {
            accept(ASSIGN_SIGN);
            if (check(SELECT_TEXT)) {
                return accept(SELECT_TEXT).getValue();
            } else if (check(TEXT)) {
                String variableName = accept(TEXT).getValue();
                if (collectionMap.containsKey(variableName)) {
                    if (check(DOT)) {
                        accept(DOT);
                        switch (lookahead.getTokenType()) {
                            case SELECT:
                                return selectFunction(variableName);
                            case WHERE:
                                return whereFunction(variableName);
                            case COUNT:
                                throw new InvalidFunctionException(lookahead.getTextPos());
                        }
                        throw new IllegalExpressionException("Unknown function");
                    }
                    return collectionMap.get(variableName);
                } else if (valueMap.containsKey(variableName))
                    throw new IllegalTypeException(lookahead.getTextPos());
                else
                    throw new UnknownVariableException(variableName, lookahead.getTextPos());
            }
            throw new IllegalExpressionException("Expected value after assignment sign in: " + lookahead.getTextPos().toString());
        }
        return null;
    }

    private void valueDeclaration() throws IllegalExpressionException, TokenReadException, VariableNameExistsException, UnknownVariableException, IllegalTypeException, SQLException {
        if (lookahead.getTokenType() != VAL)
            return;
        String name = readVariableName(VAL);
        Float value = valueAssignment();
        accept(SEMICOLON);
        valueMap.put(name, value);
    }

    private Float valueAssignment() throws IllegalExpressionException, TokenReadException, IllegalTypeException, UnknownVariableException, SQLException {
        if (check(ASSIGN_SIGN)) {
            accept(ASSIGN_SIGN);
            return Optional.of(mathOp()).orElseThrow(() -> new IllegalExpressionException("Expected value after assignment sign in: " + lookahead.getTextPos().toString()));
        }
        return null;
    }

    private Float mathOp() throws TokenReadException, IllegalTypeException, UnknownVariableException, IllegalExpressionException, SQLException {
        StringBuilder stringBuilder = new StringBuilder();
        String curVal;
        do {
            if (lookahead.getTokenType() == TEXT) {
                if (valueMap.containsKey(lookahead.getValue()))
                    curVal = valueMap.get(lookahead.getValue()).toString();
                else if (collectionMap.containsKey(lookahead.getValue())) {
                    String variableName = accept(TEXT).getValue();
                    accept(DOT);
                    accept(COUNT);
                    accept(LEFT_BRACKET);
                    accept(RIGHT_BRACKET);
                    curVal = String.valueOf(countFunction(variableName));
                }
//                    throw new IllegalTypeException(lookahead.getTextPos());
                else throw new UnknownVariableException(lookahead.getValue(), lookahead.getTextPos());
            } else
                curVal = lookahead.getValue();
            stringBuilder.append(curVal);
            nextToken();
        } while (lookahead.getTokenType() == NUMBER || lookahead.getTokenType() == MATH_OP_SIMPLE || lookahead.getTokenType() == MATH_OP_MULTI || lookahead.getTokenType() == TEXT);
        Expression calc = new ExpressionBuilder(stringBuilder.toString()).build();
        return (float) calc.evaluate();
    }

    private Float countFunction(String variableName) throws SQLException {
        return dataBase.count(DataBaseProcessing.count(collectionMap.get(variableName)));
    }

    private void variableValueChange() throws IllegalExpressionException, TokenReadException, UnknownVariableException, InvalidFunctionException, IllegalTypeException, SQLException {
        if (!check(TEXT)) {
            return;
        }
        String variable = accept(TEXT).getValue();
        if (collectionMap.containsKey(variable)) {
            String value = collectionAssignment();
            if (value == null)
                throw new IllegalExpressionException("No value assigned! ");
            accept(SEMICOLON);
            collectionMap.put(variable, value);

        } else if (valueMap.containsKey(variable)) {
            Float value = valueAssignment();
            if (value == null)
                throw new IllegalExpressionException("No value assigned! ");
            accept(SEMICOLON);
            valueMap.put(variable, value);
        } else
            throw new UnknownVariableException(variable);
    }

    private String readVariableName(TokenType tokenType) throws VariableNameExistsException, IllegalExpressionException, TokenReadException {
        accept(tokenType);
        String name = accept(TEXT).getValue();
        if (collectionMap.containsKey(name) || valueMap.containsKey(name)) {
            throw new VariableNameExistsException(name);
        }
        return name;
    }

    private String selectFunction(String variableName) throws IllegalExpressionException, TokenReadException {
        accept(SELECT);
        accept(LEFT_BRACKET);
        List<String> selectArgs = readSelectArgs();
        accept(RIGHT_BRACKET);
        return DataBaseProcessing.select(collectionMap.get(variableName), selectArgs);
    }

    private String whereFunction(String variableName) throws IllegalExpressionException, TokenReadException {
        accept(WHERE);
        accept(LEFT_BRACKET);
        String value = whereFunctionReadDecl();
        accept(RIGHT_BRACKET);
        return DataBaseProcessing.where(value);
    }

    private String whereFunctionReadDecl() throws IllegalExpressionException, TokenReadException {
        //TODO add more then one arg
        StringBuilder result = new StringBuilder();
        String whereArg;
        do {
            whereArg = readWhereArg();
        } while (whereArg != null);
        return null;
    }

    private String readWhereArg() {
        return null;
    }

    private List<String> readSelectArgs() throws IllegalExpressionException, TokenReadException {
        List<String> args = new ArrayList<>();
        String value = readArg();
        while (value != null) {
            args.add(value);
            value = readArg();
        }
        return args;
    }

    private String readArg() throws IllegalExpressionException, TokenReadException {
        if (check(COLUMN)) {
            String val = accept(COLUMN).getValue();
            if (check(COMMA))
                accept(COMMA);
            return val;
        }
        return null;
    }

    private void print() throws IllegalExpressionException, TokenReadException, UnknownVariableException, SQLException {
        if (!check(PRINT))
            return;
        accept(PRINT);
        accept(LEFT_BRACKET);
        if (check(NUMBER)) {
            float value = Float.valueOf(accept(NUMBER).getValue());
            accept(RIGHT_BRACKET);
            accept(SEMICOLON);
            System.out.println(value);
        } else if (check(TEXT)) {
            String variableName = accept(TEXT).getValue();
            accept(RIGHT_BRACKET);
            accept(SEMICOLON);
            if (collectionMap.containsKey(variableName)) {
                System.out.println(dataBase.select(collectionMap.get(variableName)));
            } else if (valueMap.containsKey(variableName)) {
                System.out.println(valueMap.get(variableName));
            } else
                throw new UnknownVariableException(variableName);
        }
    }

    private void condition() throws IllegalExpressionException, TokenReadException {
        if(!check(CONDITION))
            return;
        accept(CONDITION);

    }
}
