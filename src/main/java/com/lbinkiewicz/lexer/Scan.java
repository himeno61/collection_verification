package com.lbinkiewicz.lexer;

import com.lbinkiewicz.exceptions.lexer.IllegalNameException;
import com.lbinkiewicz.exceptions.lexer.NameOutOfBandException;
import com.lbinkiewicz.source.Source;
import com.lbinkiewicz.source.TextPos;
import com.lbinkiewicz.token.Token;
import lombok.extern.log4j.Log4j;

import static com.lbinkiewicz.token.TokenType.*;
import static java.lang.Character.*;

@Log4j
public class Scan {
    private Source source;
    private int intValue;
    public static final int MAX_LEN = 25;

    public Scan(Source source) {
        this.source = source;
        this.intValue = source.getChar();
    }

    public Token getNextToken() throws NameOutOfBandException, IndexOutOfBoundsException, IllegalNameException {
        char currentChar;
        if (intValue == -1) {
            return new Token(END_OF_FILE, "EOF", source.getTextPos());
        }
        TextPos beginningOfToken = new TextPos(source.getTextPos().getLn(), source.getTextPos().getCn());
        removeWhitespacesAndComments();

        currentChar = (char) intValue;
        switch (currentChar) {
            case '\"':
                return readTextDeclaration(beginningOfToken);
            case '\'':
                return readColumnName(beginningOfToken);
            case ';':
                intValue = source.getChar();
                return new Token(SEMICOLON, ";", beginningOfToken);
            case '(':
                intValue = source.getChar();
                return new Token(LEFT_BRACKET, "(", beginningOfToken);
            case ')':
                intValue = source.getChar();
                return new Token(RIGHT_BRACKET, ")", beginningOfToken);
            case '.':
                intValue = source.getChar();
                return new Token(DOT, ".", beginningOfToken);
            case '=':
                intValue = source.getChar();
                return new Token(ASSIGN_SIGN, "=", beginningOfToken);
            case ':':
                intValue = source.getChar();
                return new Token(COLON, ":", beginningOfToken);
            case '*':
            case '/':
                intValue = source.getChar();
                return new Token(MATH_OP_MULTI, String.valueOf(currentChar), beginningOfToken);
            case '+':
            case '-':
                intValue = source.getChar();
                return new Token(MATH_OP_SIMPLE, String.valueOf(currentChar), beginningOfToken);
            case '>':
            case '<':
                intValue = source.getChar();
                return new Token(COMPARISON_OP, String.valueOf(currentChar), beginningOfToken);
            case '{':
                intValue = source.getChar();
                return new Token(LEFT_BRACE, "{", beginningOfToken);
            case '}':
                intValue = source.getChar();
                return new Token(RIGHT_BRACE, "}", beginningOfToken);
            case ',':
                intValue = source.getChar();
                return new Token(COMMA, ",", beginningOfToken);
            case '[':
                intValue = source.getChar();
                return new Token(SQUARE_LEFT_BRACKET, "[", beginningOfToken);
            case ']':
                intValue = source.getChar();
                return new Token(SQUARE_RIGHT_BRACKET, "]", beginningOfToken);

        }
        if (isDigit(currentChar)) {
            return readVal(beginningOfToken);
        }
        if (isAlphabetic(currentChar)) {
            return readText(beginningOfToken);
        }
        if (intValue == -1) {
            return new Token(END_OF_FILE, "EOF", source.getTextPos());
        }
        throw new IllegalNameException(String.valueOf(intValue), beginningOfToken);
    }


    private Token readTextDeclaration(TextPos beginningOfToken) throws IndexOutOfBoundsException {
        StringBuilder stringBuilder = new StringBuilder();
        intValue = source.getChar();
        char c = (char) intValue;
        while (c != '"') {
            if (intValue == -1)
                throw new IndexOutOfBoundsException("Missing [\"] at the end of statement");
            // get special sign
            if (c == '\\') {
                intValue = source.getChar();
                c = (char) intValue;
            }
            stringBuilder.append(c);
            intValue = source.getChar();
            c = (char) intValue;
        }
        intValue = source.getChar();
        return new Token(SELECT_TEXT, stringBuilder.toString(), beginningOfToken);
    }

    private void removeWhitespacesAndComments() throws IndexOutOfBoundsException {
        while (skipWhitespace() || skipComment()) ;
    }

    private boolean skipWhitespace() {
        if (isWhitespace(intValue)) {
            do {
                intValue = source.getChar();
            } while (isWhitespace(intValue));
            return true;
        }
        return false;
    }

    private boolean skipComment() {
        char currentChar = (char) intValue;
        if (currentChar == '#') {
            do {
                intValue = source.getChar();
                currentChar = (char) intValue;
                if (intValue == -1)
                    throw new IndexOutOfBoundsException("Missing [#] at the end of statement");
            } while (currentChar != '#');
            intValue = source.getChar();
            return true;
        }
        return false;
    }

    private Token readColumnName(TextPos beginningOfToken) throws IndexOutOfBoundsException {
        StringBuilder stringBuilder = new StringBuilder();
        intValue = source.getChar();
        char c = (char) intValue;
        while (c != '\'') {
            stringBuilder.append(c);
            intValue = source.getChar();
            c = (char) intValue;
            if (intValue == -1)
                throw new IndexOutOfBoundsException("Missing [\'] at the end of statement");
        }
        intValue = source.getChar();
        return new Token(COLUMN, stringBuilder.toString(), beginningOfToken);
    }


    private Token readVal(TextPos beginningOfToken) {
        StringBuilder resultBuilder = new StringBuilder();
        char c = (char) intValue;
        if (c != '0') {
            while (isDigit(c)) {
                resultBuilder.append(c);
                intValue = source.getChar();
                c = (char) intValue;
            }
        } else {
            resultBuilder.append(c);
            intValue = source.getChar();
            c = (char) intValue;
        }
        if (c == '.') {
            resultBuilder.append(c);
            intValue = source.getChar();
            c = (char) intValue;
            while (isDigit(c)) {
                resultBuilder.append(c);
                intValue = source.getChar();
                c = (char) intValue;
            }
        }
        return new Token(NUMBER, resultBuilder.toString(), beginningOfToken);
    }

    private Token readText(TextPos beginningOfToken) throws NameOutOfBandException {
        StringBuilder resultBuilder = new StringBuilder();
        char c;
        while (isAlphabetic(intValue) || ((char) intValue) == '_') {
            c = (char) intValue;
            resultBuilder.append(c);
            intValue = source.getChar();
        }
        if (resultBuilder.toString().length() > MAX_LEN)
            throw new NameOutOfBandException(beginningOfToken);
        switch (resultBuilder.toString()) {
            case "Val":
                return new Token(VAL, resultBuilder.toString(), beginningOfToken);
            case "Collection":
                return new Token(COLLECTION, resultBuilder.toString(), beginningOfToken);
            case "where":
                return new Token(WHERE, resultBuilder.toString(), beginningOfToken);
            case "select":
                return new Token(SELECT, resultBuilder.toString(), beginningOfToken);
            case "count":
                return new Token(COUNT, resultBuilder.toString(), beginningOfToken);
            case "forEach":
                return new Token(FUNC2, resultBuilder.toString(), beginningOfToken);
            case "print":
                return new Token(PRINT, resultBuilder.toString(), beginningOfToken);
            case "condition":
                return new Token(CONDITION, resultBuilder.toString(), beginningOfToken);
        }
        return new Token(TEXT, resultBuilder.toString(), beginningOfToken);
    }
}
