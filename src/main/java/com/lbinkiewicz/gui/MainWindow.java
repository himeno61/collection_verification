package com.lbinkiewicz.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.Builder;
import lombok.extern.java.Log;

@Log
@Builder
public class MainWindow {
    public static void start(Stage primaryStage) throws Exception {
        Parent root;
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(MainWindow.class.getResource("/main.fxml"));
            root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("Collection Verification");
            primaryStage.sizeToScene();
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception e) {
            log.info("unable to open GUI");
        }
    }
}
