package com.lbinkiewicz.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import lombok.extern.java.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;


@Log
public class MainWindowController {


    @FXML
    private VBox mainWindow;

    @FXML
    private TextArea codeBox;

    @FXML
    private Button loadButton;

    @FXML
    private Button clearButton;

    @FXML
    private Button compileButton;


    @FXML
    void clearBoxClick(ActionEvent event) {
        codeBox.setText(null);
    }

    @FXML
    void compileClick(ActionEvent event) {
        log.info("compileClick");

        codeBox.getText();
    }

    @FXML
    void loadCodeClick(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load your code");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        Optional<File> optionalFile = Optional.ofNullable(fileChooser.showOpenDialog(mainWindow.getScene().getWindow()));
        optionalFile.ifPresent(y -> {
            try {
                codeBox.setText(String.join("\n", Files.readAllLines(Paths.get(y.getPath()))));
            } catch (IOException e) {
                log.info(y.getName() + " was not loaded");
            }
        });
    }
}
