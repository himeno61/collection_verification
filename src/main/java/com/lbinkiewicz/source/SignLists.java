package com.lbinkiewicz.source;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


@Getter
public class SignLists {
    static final List mathOpList = new ArrayList<Character>() {{
        add('/');
        add('*');
        add('+');
        add('-');
    }};
    static final List logicOpList = new ArrayList<Character>() {{
        add('>');
        add('<');
        add('|');
        add('&');
    }};
    static final List breakList = new ArrayList<Character>() {{
        add(':');
        add('\'');
        add(';');
        add('"');
        add(')');
        add('(');
        add('=');
        add('[');
        add('=');
        add('{');
        add('}');
        add(',');
        add('[');
        add(']');
    }};

    public static List getLogicOpList() {
        return logicOpList;
    }

    public static List getMathOpList() {
        return mathOpList;
    }

    public static List getBreakList() {
        return breakList;
    }
}
