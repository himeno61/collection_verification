package com.lbinkiewicz.source;


import lombok.extern.log4j.Log4j;

import java.io.IOException;
import java.io.InputStream;


@Log4j
public class Source {
    private InputStream inputStream;
    private TextPos textPos;

    public Source(InputStream inputStream) {
        this.inputStream = inputStream;
        textPos = new TextPos(1, 0);
    }

    public int getChar() {
        int c = -1;
        try {
            c = inputStream.read();
            textPos.cn++;
            if ((char) c == '\n')
                nextLine();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return c;
    }

    private void nextLine() {
        textPos.cn = 0;
        textPos.ln++;
    }

    public TextPos getTextPos() {
        return textPos;
    }
}
