package com.lbinkiewicz.source;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString(includeFieldNames = false)
@Getter
public class TextPos {
    int ln, cn;
}
