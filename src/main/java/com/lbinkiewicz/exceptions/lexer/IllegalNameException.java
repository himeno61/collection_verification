package com.lbinkiewicz.exceptions.lexer;


import com.lbinkiewicz.source.TextPos;
import lombok.extern.log4j.Log4j;

@Log4j
public class IllegalNameException extends Exception {
    public IllegalNameException(String message, TextPos textPos) {
        super(message);
        log.error("Illegal name: "+message+ " \n->at line: "+textPos.toString());
    }
}
