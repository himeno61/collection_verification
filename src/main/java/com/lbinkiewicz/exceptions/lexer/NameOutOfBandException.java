package com.lbinkiewicz.exceptions.lexer;

import com.lbinkiewicz.source.TextPos;
import lombok.extern.log4j.Log4j;

import static com.lbinkiewicz.lexer.Scan.MAX_LEN;

@Log4j
public class NameOutOfBandException extends Exception {
    public NameOutOfBandException(TextPos textPos) {
        super("Name starting at: " + textPos.toString() + " longer than limit[" + MAX_LEN + "]");
        log.error("Name starting at: " + textPos.toString() + " longer than limit[" + MAX_LEN + "]");
    }
}
