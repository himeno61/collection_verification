package com.lbinkiewicz.exceptions.parser;

import com.lbinkiewicz.source.TextPos;
import lombok.extern.log4j.Log4j;

@Log4j
public class InvalidFunctionException extends Exception {
    public InvalidFunctionException(TextPos textPos) {
        super("Can`t call this function on this type! at:"+textPos);
        log.error("Can`t call this function on this type! at:"+textPos);
    }
}
