package com.lbinkiewicz.exceptions.parser;

import com.lbinkiewicz.token.Token;
import com.lbinkiewicz.token.TokenType;
import lombok.extern.log4j.Log4j;

@Log4j
public class IllegalExpressionException extends Exception {
    public IllegalExpressionException(Token token, TokenType tokenType) {
        super("Illegal token. Expected:  " + tokenType + " at: " + token.getTextPos());
        log.error("Illegal token. Expected:  " + tokenType + " at: " + token.getTextPos());
    }

    public IllegalExpressionException(String s) {
        super(s);
        log.error(s);
    }
}
