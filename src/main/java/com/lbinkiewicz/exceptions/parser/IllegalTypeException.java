package com.lbinkiewicz.exceptions.parser;

import com.lbinkiewicz.source.TextPos;
import lombok.extern.log4j.Log4j;

@Log4j
public class IllegalTypeException extends Exception {
    public IllegalTypeException(TextPos textPos) {
        super("Can`t assign other type "+textPos.toString());
        log.error("Can`t assign other type "+textPos.toString());
    }
}
