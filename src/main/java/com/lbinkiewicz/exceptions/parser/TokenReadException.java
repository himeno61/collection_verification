package com.lbinkiewicz.exceptions.parser;

public class TokenReadException extends Exception {
    public TokenReadException(Throwable cause) {
        super(cause);
    }
}
