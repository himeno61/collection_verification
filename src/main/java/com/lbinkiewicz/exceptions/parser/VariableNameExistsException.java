package com.lbinkiewicz.exceptions.parser;

import lombok.extern.log4j.Log4j;

@Log4j
public class VariableNameExistsException extends Exception {
    public VariableNameExistsException(String name) {
        super("Variable name: " + name + " already exists!");
        log.error("Variable name: " + name + " already exists!");
    }
}
