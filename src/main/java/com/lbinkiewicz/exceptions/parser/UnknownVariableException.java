package com.lbinkiewicz.exceptions.parser;

import com.lbinkiewicz.source.TextPos;
import lombok.extern.log4j.Log4j;

@Log4j
public class UnknownVariableException extends  Exception {
    public UnknownVariableException(String text, TextPos textPos) {
        super("Unknown variable name: "+text+ " at: "+textPos);
        log.error("Unknown variable name: "+text+ " at: "+textPos);
    }

    public UnknownVariableException(String text) {
        super("Unknown variable name: "+text);
        log.error("Unknown variable name: "+text);
    }
}
