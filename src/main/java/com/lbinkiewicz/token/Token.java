package com.lbinkiewicz.token;

import com.lbinkiewicz.source.TextPos;
import lombok.*;

@ToString(includeFieldNames = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class Token {
    TokenType tokenType;
    String value;
    TextPos textPos;
}
